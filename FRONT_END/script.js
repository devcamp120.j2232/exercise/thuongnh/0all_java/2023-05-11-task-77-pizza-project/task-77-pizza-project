/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

/*** REGION 2 - Vùng gán / thực thi sự kiện cho các elements */
$(document).ready(function () {
    onPageLoading();
    $("#btn-quay-lai").on("click", onQuayLai);   // nút xác nhận thêm  
});

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
function onPageLoading() {
    "use strict"
    console.log("đã load xong thẻ body ");
    var vObjectId = {
        id: "",
        albumName: "",
    }
      // b1 thu thập dữ liệu 
    readData(vObjectId);
    console.log( vObjectId.albumName);
    // b2 kiểm tra dữ liệu 
    var vResult = getValidateData(vObjectId);
    console.log( "vResult:   " +  vResult);
    if (vResult) {
         // b3 call server
    getCallServerImageListDetail(vObjectId.id, function (response) {
        console.log("getCallServerImageListDetail detai albums" );
        $("#h1-tieu-de-card").text(vObjectId.albumName); // load tên album lên thẻ h1   h1-tieu-de-card
        console.log( response.images);
        var listImages = response.images;
        // b4 hiển thị font end 
        taoTheCardKhoaHoc(listImages, "#Recommended-to-you"); // Recommended to you: đề xuất cho bạn
    }
    );
    } else {
      getCallServerAllImages(function (response) {
        console.log("getCallServerImageListDetail detai albums" );
        console.log( response);
        var listImages = response;
        // b4 hiển thị font end 
        taoTheCardKhoaHoc(response, "#Recommended-to-you"); // Recommended to you: đề xuất cho bạn
    });

    }
    // b4 load dữ liệu ra form 
    console.log(vObjectId);
   
   
}

/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// b1 thu  thập dữ liệu 
function readData(paramObject) {
    var vStringUrl = window.location.href;
    var vUrl = new URL(vStringUrl);
    var vId = vUrl.searchParams.get("id");
    var albumName = vUrl.searchParams.get("album");
    console.log(vId);
    console.log(albumName);
    // bỏ qua dòng này nếu xảy ra lỗi;
    try {
        paramObject.id = vId;
        paramObject.albumName = albumName;
    }
    catch (paramError) {
        console.log("%c lỗi bắt được  là :" + paramError, "color:red");
    }

    return vId;
}

  // b2 kiểm tra id có tồn tại hay không
  function getValidateData(paramObject) {
    if (isNaN(paramObject.id)) {
        console.log("id không phải là số ");
        return false;
    }
    if (paramObject.id == "") {
        console.log("id là chuỗi rỗng ");
        return false;
    }
    if (paramObject.id == null) {
      console.log("id là null ");
      return false;
  }
    return true;
}

 // ấn nút quay lại 
 function onQuayLai() {
    console.log("đã ấn nút quay lại ");
    window.location.href = "albums.html";
}

// khai báo hàm ..
// truyền tham số .. object các khóa học . số bắt đầu , số kết thúc..  và ID của phần tử cần add vào
// đầu ra : các thẻ card được hiển thị .. các tham số được truyền vào thẻ card
function taoTheCardKhoaHoc(paramObject,  paramIdDivCard) {
    // paramBatDau ParamKetThuc 
    for (var i = 0; i < paramObject.length; i++) {
        // tạo ra thẻ car html 
        var vCard = $(`<div class="col-sm-3">
        <div class="card">
          <img src="` + paramObject[i].imageLink + `" alt="" style="width: 100%;">
          <div class="card-body ">
            <b class="p-mo-ta">` + paramObject[i].imageName + `</b> 
            <p> <i class="far fa-clock"></i>  ` + paramObject[i].ngayTao + ` </p>
            <p> <b>  code:  ` + paramObject[i].imageCode   + `</span> </p>
          </div>
          <div class="card-footer ">
            <div class="row">
              <div class="col-sm-12">
                <label> Mô tả: `  + paramObject[i].moTa + `</label>
              </div>
            </div>
          </div>
        </div>
      </div>`);
        // thêm thẻ vcard vào thể div có id="Recommended-to-you"
        $(vCard).appendTo(paramIdDivCard);
    }
}

// gọi server để lấy danh sách 
function getCallServerImageListDetail(paramId,paramFunction) {
    $.ajax({
        type: "GET",
        url: "http://localhost:8080/album/detail/" + paramId,
        dataType: "json",
        success: paramFunction,
        error: function (paramError) {
            console.log("lỗi gọi server", paramError.status);
        }
    });
}

function getCallServerAllImages(paramFunction){
  $.ajax({
    type: "GET",
    url: "http://localhost:8080/image/all/" ,
    dataType: "json",
    success: paramFunction,
    error: function (paramError) {
        console.log("lỗi gọi server", paramError.status);
    }
});

}