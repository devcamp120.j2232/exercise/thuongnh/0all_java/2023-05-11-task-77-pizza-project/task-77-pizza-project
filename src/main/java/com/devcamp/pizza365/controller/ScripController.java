package com.devcamp.pizza365.controller;

import java.text.Normalizer;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.CongTrinh;
import com.devcamp.pizza365.entity.Scrip;
import com.devcamp.pizza365.service.CongTrinhServise;
import com.devcamp.pizza365.service.ScripService;

@CrossOrigin
@RestController
public class ScripController {
    @Autowired
    private ScripService scripService;

    @Autowired
    private CongTrinhServise congTrinhServise;

    @Autowired

    // tất cả các công trình
    @GetMapping("/scrip/all")
    public List<Scrip> getAllCongTrinhs() {
        return scripService.getAllScrips();
    }

    @GetMapping("/get_scrip/{name}")
    public Scrip getFinByName(@PathVariable String name){
        return scripService.findByName(name);
    }

    // tạo mới và khởi tạo scrip ban đầu . quy trình là chưa , đang, đã
    // thêm 1 công trình với tên nick name
    @PostMapping("/new_scrip")
    public Object createCongTrinh(@RequestBody String name) {
        try {
            return scripService.saveScrip(name);
        } catch (Exception e) {
            return "tên thống đốc khi tạo scrip  đã tồn tại hoặc bị lỗi server";
        }
    }
    // set trạng thái thành Dang
    @PostMapping("/scrip_status_dang/{name}")
    public Object setStatus(@PathVariable String name) {
        try {
            return scripService.setStatusDang(name);
        } catch (Exception e) {
            return "tên thống đốc khi đổi status   đã tồn tại hoặc bị lỗi server";
        }
    }
    // set trang thái thành chưa 
    @PostMapping("/scrip_status_chua/{name}")
    public Object setStatusChua(@PathVariable String name) {
        try {
            return scripService.setStatusChua(name);
        } catch (Exception e) {
            return "tên thống đốc khi đổi status   đã tồn tại hoặc bị lỗi server";
        }
    }

    // đầu vào tên nick và thông tin của công trình dạng text ( nhà chính cấp 9)
    @PostMapping("/name_scrip/{name}")
    public Object findCongTrinhByName(@PathVariable String name) {
        String ketQuaTraVe = "chua gan ket qua";
        int level = -1;
        int x = -1;
        int y = -1;
        Scrip scrip = scripService.findByName(name); // lưu trữ trạng thái. scrip . công việc hiện tại cần làm. lv thực
                                                     // tế của công trình tại client
        CongTrinh congTrinh = congTrinhServise.findByName(name);
        String congTrinhCanNangCap = scrip.getCongTrinhCanNangCap();
        String getChuoiLuuScrip = scrip.getChuoiLuuScrip();
        if (getChuoiLuuScrip.equals("")) {
            ketQuaTraVe = "đã hết kịch bản xây dựng"; // hết kịch bản xây dựng
        }
        System.out.println(congTrinhCanNangCap);
        Pattern pattern = Pattern.compile("\\d+"); // < TÁCH LẤY SỐ
        Matcher matcher = pattern.matcher(congTrinhCanNangCap);// Tạo một matcher để tìm các kết quả phù hợp trong chuỗi
        // Lặp qua tất cả các kết quả phù hợp và in ra
        while (matcher.find()) {
            String number = matcher.group(); // Lấy số từ kết quả phù hợp
            level = Integer.parseInt(number); // Chuyển đổi số từ chuỗi thành số nguyên công trình cần nâng cấp
        }
        // >>> TÁCH LẤY CHUỖI
        String[] wordsToMatch = {
                "toa Thi Chinh", "nha Ket Giao", "tram Dich", "tuong Thanh", "trai Trinh Sat",
                "doanh Trai", "truong Ban", "chuong Ngua", "nha Vu Khi Bao Vay", "mo Đa", "nong Trai", "nha May Go",
                "mo Vang", "nha Kho", "hoc Vien", "trung Tam Lien Minh", "benh Vien", "cho Phien",
        };
        String matchedWord = findMatchedWord(congTrinhCanNangCap.toLowerCase(), wordsToMatch); // kết quả trả về là Type
        if (matchedWord != null) {
            System.out.println("Chuỗi khớp với từ trong danh sách: " + matchedWord);
        } else {
            System.out.println("Chuỗi không khớp với từ trong danh sách.");
        }
        System.out.println("lv: " + level);
        System.out.println("tim thay cong trinh : ");
        // sử dụng thêm lệnh if trong case
        // building = new BuildingCongTrinh(x, y, "ok", level); // các biến đã tách từ
        // text ra các biến timeout
        if (scrip.getStatus().equals("dang")) {
            ketQuaTraVe = "dang";
        }
        if (scrip.getStatus().equals("timeout")) {
            String timeOutToaDo = scripService.switchCase(matchedWord, level, x, y, congTrinh, scrip, ketQuaTraVe);  // tọa độ 
            ketQuaTraVe = "timeout=" + timeOutToaDo;
        }
        if (scrip.getStatus().equals("chua")) {
            System.out.println("chua : ");
            ketQuaTraVe = scripService.switchCase(matchedWord, level, x, y, congTrinh, scrip, ketQuaTraVe);  // tọa độ 
            // return new ResponseEntity<>("Loại công trình không hợp lệ",
            // HttpStatus.BAD_REQUEST);
            // }
        }

        return ketQuaTraVe;
    }

    // phương thức sẽ trả về để client biết phải làm gì
    // đầu vào là phương thức hỏi.., đầu ra đưa ra trạng thái chờ nếu có kết quả thì
    // đưa ra tọa độ
    // nếu có trạng thái là time out thì client kiểm tra tiến độ hoàn thành.. cập
    // nhập dữ liệu lên CongTrinh

    // CÁC PHƯƠNG THỨC BÊN NGOÀI . KHÔNG XỬ LÝ CONTROLLER
    public static String findMatchedWord(String str, String[] wordsToMatch) {
        // Loại bỏ dấu từ chuỗi str
        str = removeDiacritics(str);

        for (String word : wordsToMatch) {
            // Loại bỏ dấu từ từ trong danh sách wordsToMatch
            String wordWithoutDiacritics = removeDiacritics(word.toLowerCase());
            if (str.contains(wordWithoutDiacritics)) {
                return word.replaceAll("\\s+", "");
            }
        }
        return null;
    }

    public static String removeDiacritics(String str) {
        str = Normalizer.normalize(str, Normalizer.Form.NFD);
        str = str.replaceAll("\\p{M}", "");
        return str;
    }

    public static String result = "";

    public static String xoaChuoiDauTienSQL(Scrip scrip) {

        String input = scrip.getChuoiLuuScrip();
        String[] parts = input.split("\\|"); // Tách chuỗi sau mỗi dấu "|"
        int length = parts.length;// Xác định số phần tử của mảng
        if (length > 0) {
            String[] trimmedParts = new String[length - 1]; // Tạo mảng mới để lưu các phần tử sau khi loại bỏ phần tử
                                                            // đầu tiên
            // Sao chép các phần tử từ mảng parts vào mảng mới, bỏ qua ph ần tử đầu tiên
            for (int i = 1; i < length; i++) {
                trimmedParts[i - 1] = parts[i];
            }
            result = String.join("|", trimmedParts); // scrip cắt đi đoạn đầu tiên nếu lv thực tế cao hơn lv scrp
        } else {
            result = "het chuoi";
        }

        return result;
    }

    // Phương thức static để lấy chuỗi đầu tiên sau khi đã tách
    public static String getChuoiDau(String input) {
        String[] parts = input.split("\\|"); // Tách chuỗi sau mỗi dấu "|"
        String firstString = parts[0]; // Lấy chuỗi đầu tiên sau khi đã tách
        return firstString;
    }

} // >>> VÒNG CUỐI

// ĐƯA RA 1 TRONNG 4 TRẠNG THÁI , trạng thái chưa nâng cấp sẽ kèm theo tọa độ
// khi lv kịch bản lớn hơn lv thực tế

// lấy thông tin của công trình qua phương thức get và lấy thông tin kịch bản
// So sámh kịch bản sẽ xóa đi nếu lv công trình đạt yêu cầu
// chưa nâng cấp , đang nâng cấp , time out , da (chua,dang,timeout, da)
// trạng thái đầu là (chưa nâng cấp )
// tìm nick name sau đó lấy dữ liệu scrip để nâng cấp . nâng cấp xong chuyển
// trạng thái sang ( đang nâng cấp ) sét thời gian đếm ngược
// thời gian đếm ngược bằng không thì tiến hành set sang (time out) hết thời
// gian
// client tiến hành kiểm tra nếu kiểm tra đúng là đã xong tiến hành so sánh
// level hiện tại so với level trên scrip . nếu bằng tiến hành chuyển trạng thái
// done
// nếu bé hơn chuyển sang trạng thái chưa nâng cấp

// nong Trai>7 / doanh Trai>7/trung Tam Lien Minh > 8/ hoc Vien >9/ chuong Ngua
// >10 / benh Vien >10/ nha Kho >11/ nha May Go >12/ truong ban >12/
// trung Tam Lien Minh > 13/ mo Vang > 13/ cho Phien > 13/ mo Vang > 14/ hoc
// Vien > 15/nong Trai > 16/ doanh Trai > 16/ benh Vien > 16
