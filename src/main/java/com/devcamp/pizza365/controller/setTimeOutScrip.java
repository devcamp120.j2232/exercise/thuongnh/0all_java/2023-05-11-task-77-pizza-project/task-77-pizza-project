package com.devcamp.pizza365.controller;

import java.text.Normalizer;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.Scrip;
import com.devcamp.pizza365.repository.ScripRepository;
import com.devcamp.pizza365.service.ScripService;

@CrossOrigin
@RestController
public class setTimeOutScrip {

    @Autowired
    private ScripService scripService;
    @Autowired
    private ScripRepository scripRepository;

    String ketQuaTraVe = "KHông tìm thấy nick name hoặc là Lỗi cú pháp ";
    int unixtime = 0;
    // thực hiện khi clien ấn nâng cấp 
    // đầu vào là tên nick + thời gian cần nâng cấp dạng số nguyên 

    @PostMapping("/set_timeout_scrip/{name}")
    public Object createCongTrinh(@RequestBody String time2, @PathVariable String name) {
        // tìm đối tượng scrip theo tên
        String time = time2.replaceAll("\\n", "");

        Scrip scrip = scripService.findByName(name);
        try {
            // 1 set thời gian vào data base . với định dạng 1 ngày 20:49:24 + thêm ngày
            // hiện tại
             //
            // 2 set trạng thái là đang nâng cấp  ==> thời gian sẽ có 1 hàm so sánh riêng ở hàm
            // main
            String regex = ".*\\b(\\d{2}:\\d{2}:\\d{2})\\b.*";
            Matcher matcher = Pattern.compile(regex).matcher(time);
            boolean isTime = matcher.matches(); // phân tích có khớp hay không
            System.out.println("Chuỗi '" + time + "' " + (isTime ? "khớp" : "không khớp") + " với biểu thức regex.");
            // kiểm tra thời gian có đúng định dạng không
            if (isTime) {
                // đúng định dạng tức là có định dạng hh::mm::ss
                String[] parts = time.split(" ngày ");
                if (parts.length > 1) {
                    // có đơn vị là ngày
                    String numberBeforeNgay = parts[0]; // ngày
                    String timeThucTe = parts[1]; // giờ
                    // lấy unix time
                    long unitTime = scripService.changeTimeToUnixInDay(numberBeforeNgay, timeThucTe);
                    System.out.println("thoi gian unix da cong voi time rok :  " + unitTime);  // đã cộng với ngày thực tế 
                    Instant instant = Instant.ofEpochSecond(unitTime);
                    // Chuyển đổi đối tượng Instant thành LocalDateTime
                    LocalDateTime dateTime = LocalDateTime.ofInstant(instant, ZoneOffset.ofHours(7));
                    System.out.println("Thời gian tương ứng: " + dateTime);
                    scrip.setTimeDamNguoc(unitTime);
                    scripRepository.save(scrip);
                    ketQuaTraVe = "Có Ngày Đã cập nhập thời gian ";

                } else {
                    // chỉ có giờ
                    Matcher matcher2 = Pattern.compile("\\b\\d{2}:\\d{2}:\\d{2}\\b").matcher(time);
                    String timeSegment = matcher2.find() ? matcher2.group()
                            : "Không tìm thấy định dạng thời gian trong chuỗi."; // chỉ mỗi giờ
                    System.out.println("Đoạn thời gian: " + timeSegment); // chỉ mỗi giờ
                    long unitTime = scripService.changeTimeToUnixNoDay(timeSegment);
                    System.out.println("thoi gian unix da cong voi time rok :  " + unitTime);  // đã cộng với ngày thực tế 
                    Instant instant = Instant.ofEpochSecond(unitTime);
                    // Chuyển đổi đối tượng Instant thành LocalDateTime
                    LocalDateTime dateTime = LocalDateTime.ofInstant(instant, ZoneOffset.ofHours(7));
                    System.out.println("Thời gian tương ứng: " + dateTime);
                    scrip.setStatus("dang");
                    scrip.setTimeDamNguoc(unitTime);
                    scripRepository.save(scrip);
                    ketQuaTraVe = "CHỉ có Giờ Đã cập nhập thời gian ";
                }

            } else {
                // sai định dạng
                ketQuaTraVe = "định dạng thời gian có vấn đè ";
                System.out.println("định dạng thời gian có vấn đè ");
            }

           
            
            return ketQuaTraVe;

        } catch (Exception e) {
            return ketQuaTraVe + e;
        }
    }

    // CÁC PHƯƠNG THỨC BÊN NGOÀI . KHÔNG XỬ LÝ CONTROLLER
    public static String findMatchedWord(String str, String[] wordsToMatch) {
        // Loại bỏ dấu từ chuỗi str
        str = removeDiacritics(str);

        for (String word : wordsToMatch) {
            // Loại bỏ dấu từ từ trong danh sách wordsToMatch
            String wordWithoutDiacritics = removeDiacritics(word.toLowerCase());
            if (str.contains(wordWithoutDiacritics)) {
                return word.replaceAll("\\s+", "");
            }
        }
        return null;
    }

    public static String removeDiacritics(String str) {
        str = Normalizer.normalize(str, Normalizer.Form.NFD);
        str = str.replaceAll("\\p{M}", "");
        return str;
    }
}
