package com.devcamp.pizza365.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.pizza365.entity.BuildingCongTrinh;
import com.devcamp.pizza365.entity.CongTrinh;
import com.devcamp.pizza365.repository.CongTrinhRepository;
import com.devcamp.pizza365.service.CongTrinhServise;

import java.util.List;

@CrossOrigin
@RestController
public class CongTrinhController {

    private static final Logger logger = LoggerFactory.getLogger(CongTrinhController.class);

    @Autowired
    private CongTrinhServise congTrinhService;
    @Autowired
    private CongTrinhRepository congTrinhRepository;

    // tất cả các công trình
    @GetMapping("/cong_trinh/all")
    public List<CongTrinh> getAllCongTrinhs() {

        logger.info("Endpoint /cong_trinh/all is called."); // Log the call to the endpoint
        return congTrinhService.getAllCongTrinhs();
    }

    // gọi 1 công trình theo ID
    @GetMapping("/cong_trinh/{id}")
    public CongTrinh getCongTrinhById(@PathVariable Long id) {
        return congTrinhService.getCongTrinhById(id);
    }

    // Phương thức tìm công trình qua name
    @GetMapping("/name_cong_trinh/{name}")
    public CongTrinh findCongTrinhByName(@PathVariable String name) {
        return congTrinhService.findByName(name);
    }

    // lấy dữ liệu từ data base trả về tọa độ . level , góc kéo
    @GetMapping("/get_cong_trinh/{name}/{buildingType}/{properType}")
    public ResponseEntity<String> getBuildingDetailsOfCongTrinhByName(@PathVariable String name,
            @PathVariable String buildingType, @PathVariable String properType) {
        logger.info("Endpoint /cong_trinh/{name}/{buildingType} is called."); // Log the call to the endpoint
        String buildingDetails = congTrinhService.getBuildingDetailsOfCongTrinhByName(name, buildingType, properType);
        if (buildingDetails != null) {
            return ResponseEntity.ok(buildingDetails);
        } else {
            return ResponseEntity.notFound().build();
        }
    }

    // sửa lại công trình
    // {
    // "x": 45,
    // "y": 487,
    // "hanhDong": "ok",
    // "level": 4
    // }

    @PostMapping("/set_cong_trinh/{name}/{type}")
    public ResponseEntity<String> setBuildingType(@PathVariable String name, @PathVariable String type,
            @RequestBody BuildingCongTrinh request) {
        // tìm công trình cần lưu qua tên
        CongTrinh congTrinh = congTrinhRepository.findByName(name);
        // nếu tìm thâys thì so sánh kiểu
        if (congTrinh != null) {
            BuildingCongTrinh building = null;
            building = new BuildingCongTrinh(request.getX(), request.getY(), request.gethanhDong(),
                    request.getLevel());
            switch (type) {
                case "toaThiChinh":
                    congTrinh.setNhaChinh(building);
                    break;
                case "benhVien":
                    congTrinh.setBenhVien(building);
                    break;
                case "nhaKetGiao":
                    congTrinh.setNhaKetGiao(building);
                    break;
                case "tramDich":
                    congTrinh.setTramDich(building);
                    break;
                case "tuongThanh":
                    congTrinh.setTuongThanh(building);
                    break;
                case "traiTrinhSat":
                    congTrinh.setTraiTrinhSat(building);
                    break;
                case "doanhTrai":
                    congTrinh.setBoBinh(building);
                    break;
                case "cungThu":
                    congTrinh.setCungThu(building);
                    break;
                case "chuongNgua":
                    congTrinh.setKyBinh(building);
                    break;
                case "nhaVuKhiBaoVay":
                    congTrinh.setLinhBanDa(building);
                    break;
                case "moDa":
                    congTrinh.setMoDa(building);
                    break;
                case "nongTrai":
                    congTrinh.setMoNgo(building);
                    break;
                case "nhaMayGo":
                    congTrinh.setMoGo(building);
                    break;
                case "moVang":
                    congTrinh.setMoVang(building);
                    break;
                case "nhaKho":
                    congTrinh.setNhaKho(building);
                    break;
                case "nhaNghienCuu":
                    congTrinh.setNhaNghienCuu(building);
                    break;
                case "trungTamLienMinh":
                    congTrinh.setTrungTamLienMinh(building);
                    break;
                case "biaKyNiem":
                    congTrinh.setBiaKyNiem(building);
                    break;
                default:
                    return new ResponseEntity<>("Loại công trình không hợp lệ", HttpStatus.BAD_REQUEST);
            }
            congTrinhRepository.save(congTrinh);
            return new ResponseEntity<>("Thuộc tính " + type + " đã được cập nhật thành công!", HttpStatus.OK);
        }
        return new ResponseEntity<>("Công trình không tồn tại hoặc không có giá trị", HttpStatus.NOT_FOUND);
    }

    // thêm công trình
    @PostMapping("/cong_trinh")
    public CongTrinh createCongTrinh(@RequestBody CongTrinh CongTrinh) {
        return congTrinhService.saveCongTrinh(CongTrinh);
    }
    // thêm 1 công trình với tên nick name
    @PostMapping("/new_cong_trinh")
    public Object createCongTrinh(@RequestBody String name) {
        String result = name.replaceAll("\\n", "");
        try {

            return congTrinhService.saveCongTrinh(result);
        } catch (Exception e) {
            // TODO: handle exception
            return "tên thống đốc đã tồn tại hoặc bị lỗi server";
        }
        
    }

    @DeleteMapping("/cong_trinh/{id}")
    public void deleteCongTrinhById(@PathVariable Long id) {
        congTrinhService.deleteCongTrinhById(id);
    }

}