package com.devcamp.pizza365.controller;

import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import net.sourceforge.tess4j.ITesseract;
import net.sourceforge.tess4j.Tesseract;
import net.sourceforge.tess4j.TesseractException;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

import org.springframework.http.HttpHeaders;

@CrossOrigin
@RestController
public class imageToText {


    private final RestTemplate restTemplate;

    public imageToText(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @PostMapping("/predict")
    public ResponseEntity<String> predict(@RequestBody String base64Image) {
        String flaskServiceUrl = "http://localhost:5000/predict";  // URL to your Flask service

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/json");

        // Create request body
        Map<String, String> requestBody = new HashMap<>();
        requestBody.put("image", base64Image);

        HttpEntity<Map<String, String>> request = new HttpEntity<>(requestBody, headers);

        ResponseEntity<String> response = restTemplate.postForEntity(flaskServiceUrl, request, String.class);
        System.out.println(response.toString());

        return ResponseEntity.ok(response.getBody());
    }

    @PostMapping("/image_text")
    public Object getImageToText(@RequestBody String base) {
        String text = "chua gan ket qua";

        // Giải mã base64 thành mảng byte
        byte[] imageBytes = Base64.getDecoder().decode(base);
        // 3 Chuyển đổi mảng byte thành BufferedImage:
        BufferedImage bufferedImage = null;
        try (ByteArrayInputStream bis = new ByteArrayInputStream(imageBytes)) {
            bufferedImage = ImageIO.read(bis);
        } catch (IOException e) {
            e.printStackTrace();

        }
        // 4 Nhận dạng văn bản bằng Tesseract:
        ITesseract tesseract = new Tesseract();
        try {
            tesseract.setDatapath("C:\\Program Files\\Tesseract-OCR\\tessdata"); // Thay đổi đường dẫn cho phù hợp với
                                                                                 // hệ
                                                                                 // thống của bạn
            text = tesseract.doOCR(bufferedImage);
            System.out.println("Text extracted from image:\n" + text);
        } catch (TesseractException e) {
            e.printStackTrace();
        }
        return text;
    }

   

}
