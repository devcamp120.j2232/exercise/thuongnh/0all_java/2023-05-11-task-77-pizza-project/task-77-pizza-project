package com.devcamp.pizza365.controller;

import java.text.Normalizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.pizza365.entity.BuildingCongTrinh;
import com.devcamp.pizza365.entity.CongTrinh;
import com.devcamp.pizza365.repository.CongTrinhRepository;
// cái này để lấy tọa độ công trình và cấp độ 
@RestController
public class TextCongTrinh {
    // private TextService textService;
    // 1 phân tích type { done}
    // 2 phân tích lv, tọa độ từ content chỉ có lv cao hơn mưới được lưu
    // 3 lưu vào data base
    @Autowired
    private CongTrinhRepository congTrinhRepository;

    
    // llưu theo id gạme 
    @PostMapping("/saveText/{name}")
    public ResponseEntity<String> saveText(@PathVariable String name, @RequestBody String content) {

        int x = -1;
        int y = -1;
        int level = -1;

        String str = content;  // dữ liệu text ảnh chụp màn hình + tọa đô xy 
        String[] wordsToMatch = {
                "toa Thi Chinh", "nha Ket Giao", "tram Dich", "tuong Thanh", "trai Trinh Sat",
                "doanh Trai", "truong Ban", "chuong Ngua", "nha Vu Khi Bao Vay", "mo Đa", "nong Trai", "nha May Go",
                "mo Vang", "nha Kho", "hoc Vien", "trung Tam Lien Minh",  "benh Vien", "cho Phien",  
        };
        String matchedWord = findMatchedWord(str.toLowerCase(), wordsToMatch); // kết quả trả về là Type
        if (matchedWord != null) {
            System.out.println("Chuỗi khớp với từ trong danh sách: " + matchedWord);
        } else {
            System.out.println("Chuỗi không khớp với từ trong danh sách.");
        }
        // Tách chuỗi thành mảng các phần tử dựa trên dấu "='"
        String[] parts = content.split("=");
        if (parts.length == 3) {
            String input = parts[0].trim(); // Chuỗi
            // Tạo một biểu thức chính quy để tìm các số trong chuỗi
            Pattern pattern = Pattern.compile("\\d+");

            // Tạo một matcher để tìm các kết quả phù hợp trong chuỗi
            Matcher matcher = pattern.matcher(input);
            // Lặp qua tất cả các kết quả phù hợp và in ra

            while (matcher.find()) {
                String number = matcher.group(); // Lấy số từ kết quả phù hợp
                level = Integer.parseInt(number); // Chuyển đổi số từ chuỗi thành số nguyên
            }
            x = Integer.parseInt(parts[1].trim()); // X
            y = Integer.parseInt(parts[2].trim()); // Y

            // In ra các phần tử đã tách được
            System.out.println("lv: " + level);
            System.out.println("X: " + x);
            System.out.println("Y: " + y);
        } else {
            System.out.println("Không thể cắt được chuỗi hoặc không đúng định dạng.");
        }

        // Text savedText = textService.saveText(content);
        System.out.println("Nội dung đoạn văn bản: " + content);
        System.out.println("nick name  " + name);


        // >>>>>>>
        String result = name.replaceAll("\\n", "");
        CongTrinh congTrinh = congTrinhRepository.findByName(result);
        // nếu tìm thấy công trình . thì so sánh công trình đó với level
        if (congTrinh != null) {
           // BuildingCongTrinh building = null;// khởi tạo
            BuildingCongTrinh building = new BuildingCongTrinh();
            System.out.println("tim thay cong trinh : " );

            // sử dụng thêm lệnh if trong case
            
            switch (matchedWord) {
                case "toaThiChinh":
                    int serverCongTrinh = congTrinh.getNhaChinh().getLevel(); // cần sửa
                    // level của clien và int
                    if (level > serverCongTrinh) {      
                        building = new BuildingCongTrinh(x + 15, y - 49, "ok", level); // các biến đã tách từ text ra các biến    
                                      congTrinh.setNhaChinh(building); // cần sửa
                    }
                    break;
                case "benhVien":
                    // level của clien và int
                    if (level > congTrinh.getBenhVien().getLevel()) {
                        building = new BuildingCongTrinh(x, y, "ok", level); // các biến đã tách từ text ra các biến
                        congTrinh.setBenhVien(building); // cần sửa
                    }
                    break;
                case "nhaKetGiao":
                    // level của clien và int
                    if (level > congTrinh.getNhaKetGiao().getLevel()) {
                        building = new BuildingCongTrinh(x, y, "ok", level); // các biến đã tách từ text ra các biến
                        congTrinh.setNhaKetGiao(building);// cần sửa
                    }
                    break;
                case "tramDich":
                    // level của clien và int
                    if (level > congTrinh.getTramDich().getLevel()) {
                        building = new BuildingCongTrinh(x, y, "ok", level); // các biến đã tách từ text ra các biến
                        congTrinh.setTramDich(building); // cần sửa
                    }
                    break;
                case "tuongThanh":
                    // level của clien và int   555 331
                    if (level > congTrinh.getTuongThanh().getLevel()) {
                        building = new BuildingCongTrinh(x, y, "ok", level); // các biến đã tách từ text ra các biến
                        congTrinh.setTuongThanh(building);// cần sửa
                    }
                    break;
                case "traiTrinhSat":
                    // level của clien và int
                    if (level > congTrinh.getTraiTrinhSat().getLevel()) {
                        building = new BuildingCongTrinh(x, y -55, "ok", level); // các biến đã tách từ text ra các biến
                        congTrinh.setTraiTrinhSat(building);// cần sửa
                    }
                    break;
                case "doanhTrai":
                    // level của clien và int
                    if (level > congTrinh.getBoBinh().getLevel()) {
                        building = new BuildingCongTrinh(x, y -55, "ok", level); // các biến đã tách từ text ra các biến
                        congTrinh.setBoBinh(building); // cần sửa
                    }
                    break;
                case "truongBan":
                    // level của clien và int
                    if (level > congTrinh.getCungThu().getLevel()) {
                        building = new BuildingCongTrinh(x, y -55, "ok", level); // các biến đã tách từ text ra các biến
                        congTrinh.setCungThu(building);// cần sửa
                    }
                    break;
                case "chuongNgua":
                    // level của clien và int
                    if (level > congTrinh.getKyBinh().getLevel()) {
                        building = new BuildingCongTrinh(x, y -55, "ok", level); // các biến đã tách từ text ra các biến
                        congTrinh.setKyBinh(building); // cần sửa
                    }
                    break;
                case "nhaVuKhiBaoVay":
                    // level của clien và int
                    if (level > congTrinh.getLinhBanDa().getLevel()) {
                        building = new BuildingCongTrinh(x, y -55, "ok", level); // các biến đã tách từ text ra các biến
                        congTrinh.setLinhBanDa(building);// cần sửa
                    }
                    break;
                case "moĐa":
                    // level của clien và int
                    if (level > congTrinh.getMoDa().getLevel()) {
                        building = new BuildingCongTrinh(x, y -55, "ok", level); // các biến đã tách từ text ra các biến
                        congTrinh.setMoDa(building); // cần sửa
                    }
                    break;
                case "nongTrai":
                    // level của clien và int
                    if (level > congTrinh.getMoNgo().getLevel()) {
                        building = new BuildingCongTrinh(x, y -55, "ok", level); // các biến đã tách từ text ra các biến
                        congTrinh.setMoNgo(building);// cần sửa
                    }
                    break;
                case "nhaMayGo":
                    // level của clien và int
                    if (level > congTrinh.getMoGo().getLevel()) {
                        building = new BuildingCongTrinh(x, y -55, "ok", level); // các biến đã tách từ text ra các biến
                        congTrinh.setMoGo(building); // cần sửa
                    }
                    break;
                case "moVang":
                    // level của clien và int
                    if (level > congTrinh.getMoVang().getLevel()) {
                        building = new BuildingCongTrinh(x, y -55, "ok", level); // các biến đã tách từ text ra các biến
                        congTrinh.setMoVang(building); // cần sửa
                    }
                    break;
                case "nhaKho":
                    // level của clien và int
                    if (level > congTrinh.getNhaKho().getLevel()) {
                        building = new BuildingCongTrinh(x -26, y - 48 , "ok", level); // các biến đã tách từ text ra các biến
                        congTrinh.setNhaKho(building);// cần sửa
                    }
                    break;
                case "hocVien":
                    // level của clien và int
                    if (level > congTrinh.getNhaNghienCuu().getLevel()) {
                        building = new BuildingCongTrinh(x, y -55, "ok", level); // các biến đã tách từ text ra các biến
                        congTrinh.setNhaNghienCuu(building);// cần sửa
                    }
                    break;
                case "trungTamLienMinh":
                    // level của clien và int
                    if (level > congTrinh.getTrungTamLienMinh().getLevel()) {
                        building = new BuildingCongTrinh(x, y -55, "ok", level); // các biến đã tách từ text ra các biến
                        congTrinh.setTrungTamLienMinh(building); // cần sửa
                    }
                    break;
                case "choPhien":
                    // level của clien và int
                    if (level > congTrinh.getBiaKyNiem().getLevel()) {
                        building = new BuildingCongTrinh(x + 15, y - 49, "ok", level); // các biến đã tách từ text ra các biến  
                        congTrinh.setBiaKyNiem(building); // // thực chất là chợ pphieen 
                    }
                    break;
                default:
                    return new ResponseEntity<>("Loại công trình không hợp lệ", HttpStatus.BAD_REQUEST);
            }
            congTrinhRepository.save(congTrinh);
            System.out.println("Thuộc tính " + matchedWord + " đã được cập nhật thành công!");
            return new ResponseEntity<>("Thuộc tính " + matchedWord + " đã được cập nhật thành công!", HttpStatus.OK);
        }
        return new ResponseEntity<>("không tìm thấy nick name", HttpStatus.NOT_FOUND);

    }

    // các phương thức cần gọi
    public static String findMatchedWord(String str, String[] wordsToMatch) {
        // Loại bỏ dấu từ chuỗi str
        str = removeDiacritics(str);

        for (String word : wordsToMatch) {
            // Loại bỏ dấu từ từ trong danh sách wordsToMatch
            String wordWithoutDiacritics = removeDiacritics(word.toLowerCase());
            if (str.contains(wordWithoutDiacritics)) {
                return word.replaceAll("\\s+", "");
            }
        }
        return null;
    }

    public static String removeDiacritics(String str) {
        str = Normalizer.normalize(str, Normalizer.Form.NFD);
        str = str.replaceAll("\\p{M}", "");
        return str;
    }

}

// "toaThiChinh"
// "nhaKetGiao"
// "tramDich"
// "tuongThanh"
// "traiTrinhSat"
// "doanhTrai"
// "cungThu"
// "chuongNgua"
// "nhaVuKhiBaoVay"
// "moDa"
// "nongTTrai"
// "nhaMayGo"
// "moVang"
// "nhaKho"
// "nhaNghienCuu"
// "trungTamLienMinh"
// "biaKyNiem"
