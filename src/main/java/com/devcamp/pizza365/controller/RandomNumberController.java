package com.devcamp.pizza365.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import java.util.Random;

@RestController
public class RandomNumberController {

    @GetMapping("/random/{a}/{b}")
    public int getRandomNumber(@PathVariable int a, @PathVariable int b) {
        // Kiểm tra xem giá trị của a và b
        if (a >= b) {
            throw new IllegalArgumentException("a phải nhỏ hơn b");
        }

        // Tạo một số ngẫu nhiên từ a đến b
        Random random = new Random();
        return random.nextInt(b - a + 1) + a;
    }
}
