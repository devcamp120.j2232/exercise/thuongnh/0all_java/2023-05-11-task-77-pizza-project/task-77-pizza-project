package com.devcamp.pizza365.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.pizza365.entity.ScripNhaChinh;

public interface ScripRepositoryNhaChinh extends JpaRepository<ScripNhaChinh, Long> {
      // Phương thức tìm công trình qua name
      ScripNhaChinh findByName(String name);
}