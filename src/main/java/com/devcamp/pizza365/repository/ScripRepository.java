package com.devcamp.pizza365.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.pizza365.entity.Scrip;

public interface ScripRepository extends JpaRepository<Scrip, Long> {
      // Phương thức tìm công trình qua name
      Scrip findByName(String name);
}