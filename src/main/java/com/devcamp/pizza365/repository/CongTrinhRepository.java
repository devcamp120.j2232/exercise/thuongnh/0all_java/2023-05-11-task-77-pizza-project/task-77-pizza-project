package com.devcamp.pizza365.repository;
import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.pizza365.entity.CongTrinh;

public interface CongTrinhRepository extends JpaRepository<CongTrinh, Long> {
   
      // Phương thức tìm công trình qua name
      CongTrinh findByName(String name);
}