package com.devcamp.pizza365;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.List;
import java.util.Random;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.devcamp.pizza365.entity.Scrip;
import com.devcamp.pizza365.entity.ScripNhaChinh;
import com.devcamp.pizza365.service.ScripService;
import com.devcamp.pizza365.service.ScripServiceNhaChinh;

@SpringBootApplication
public class VoucherCrudApplication {

    public static void main(String[] args) {
        // SpringApplication.run(VoucherCrudApplication.class, args);
        ApplicationContext context = SpringApplication.run(VoucherCrudApplication.class, args);
        ScripService scripService = context.getBean(ScripService.class);
        ScripServiceNhaChinh scripServiceNhaChinh = context.getBean(ScripServiceNhaChinh.class);


        // Bây giờ bạn có thể sử dụng ScripService trong hàm main
        // List<Scrip> listScrip = scripService.getAllScrips();
        // System.out.println("list all scrip :>>>>>> " + listScrip.toString() );
        // run();
        soSanhTime(scripService, scripServiceNhaChinh);  // set thời gian cho scrip 

    }

    public static void run() {
        // Vòng lặp vô hạn để in ra console mỗi giây
        while (true) {
            Random random = new Random();
            int intrandom = random.nextInt(2);
            System.out.println("in lai sau 6 giay.          " + intrandom);
            long currentUnixTime = Instant.now().getEpochSecond(); // time unix
            System.out.println("time unix: " + currentUnixTime);
            Instant instant = Instant.ofEpochSecond(currentUnixTime);
            // Chuyển đổi đối tượng Instant thành LocalDateTime
            LocalDateTime dateTime = LocalDateTime.ofInstant(instant, ZoneOffset.ofHours(7)); // time dạng ngày tháng
                                                                                              // năm
            // In ra định dạng ngày giờ
            System.out.println("Thoi gian tuong ung: " + dateTime);
            try {
                // Ngủ một giây trước khi in ra console tiếp theo
                Thread.sleep(6000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    // call all dữ liệu unix time từ data base . so sánh từng unix time với time
    // hiện tại . nếu unix time rok > now thì set trạng thái của chính nick đó thành
    // time out
    // lưu vào List List<Scrip> sau đó lặp toàn bộ bằng vòng lặp wile để so sánh với
    // time now .
    public static void soSanhTime(ScripService scripService, ScripServiceNhaChinh scripServiceNhaChinh ) {
        while (true) {
             // Thực hiện công việc với mỗi đối tượng Scrip ở đây
            long nowUnixTime = Instant.now().getEpochSecond(); // time unix
            System.out.println("thoi gian tuong ung: ");
            System.out.println("time unix: " + nowUnixTime);
            List<Scrip> listScrip = scripService.getAllScrips();
            for (int i = 0; i < listScrip.size(); i++) {
                Scrip scrip = listScrip.get(i);
                String Status = scrip.getStatus();
               
                if (scrip.getTimeDamNguoc() < nowUnixTime && Status.equals("dang")) {
                    System.out.println("/tiến hành set trạng thái thành time out neu trang thai la dang nang cap: ");
                    System.out.println(Status);
                    scrip.setStatus("timeout"); // tiến hành set trạng thái thành time out

                }
                long soPhutConLai = (scrip.getTimeDamNguoc() - nowUnixTime) / 60; // tính ra số phút
                if(soPhutConLai < 0 || scrip.getTimeDamNguoc() ==  3432939584L){
                    long soPhut = 0;
                    scrip.setPhutDemNguoc( soPhut);
                }else{
                    scrip.setPhutDemNguoc(soPhutConLai); // tính ra số phút
                }
                
                scripService.saveTuTao(scrip);
            }

             // Thực hiện công việc với mỗi đối tượng Scrip ở đây
             long nowUnixTime2 = Instant.now().getEpochSecond(); // time unix
             System.out.println("thoi gian tuong ung nha chinh: ");
             System.out.println("time unix nha chinh : " + nowUnixTime2);
             List<ScripNhaChinh> listScripNhaChinh = scripServiceNhaChinh.getAllScrips();
             System.out.print("lenht : " + listScripNhaChinh.size());
             for (int j = 0; j < listScripNhaChinh.size(); j++) {
               
                 ScripNhaChinh scripNhaChinh = listScripNhaChinh.get(j);
                 String Status = scripNhaChinh.getStatus();
                
                 if (scripNhaChinh.getTimeDamNguoc() < nowUnixTime2 && Status.equals("dang")) {
                     System.out.println("/tiến hành set trạng thái thành time out neu trang thai la dang nang cap: ");
                     System.out.println(Status);
                     scripNhaChinh.setStatus("timeout"); // tiến hành set trạng thái thành time out
 
                 }
                 long soPhutConLai = (scripNhaChinh.getTimeDamNguoc() - nowUnixTime2) / 60; // tính ra số phút
                 if(soPhutConLai < 0 || scripNhaChinh.getTimeDamNguoc() ==  3432939584L){
                     long soPhut = 0;
                     scripNhaChinh.setPhutDemNguoc( soPhut);
                 }else{
                    scripNhaChinh.setPhutDemNguoc(soPhutConLai); // tính ra số phút
                 }
                 
                 scripServiceNhaChinh.saveTuTao(scripNhaChinh);
             }


            try {
                // Ngủ một giây trước khi in ra console tiếp theo
                Thread.sleep(7000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

   


}
