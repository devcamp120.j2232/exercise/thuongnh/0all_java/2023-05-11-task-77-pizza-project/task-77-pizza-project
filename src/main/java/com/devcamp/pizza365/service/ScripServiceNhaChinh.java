package com.devcamp.pizza365.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.CongTrinh;
import com.devcamp.pizza365.entity.ScripNhaChinh;
import com.devcamp.pizza365.repository.ScripRepositoryNhaChinh;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneOffset;
import java.util.List;

@Service
public class ScripServiceNhaChinh {

    @Autowired
    private ScripRepositoryNhaChinh scripRepository;

    public List<ScripNhaChinh> getAllScrips() {
        return scripRepository.findAll();
    }

    public Object saveTuTao(ScripNhaChinh scrip) {
        return scripRepository.save(scrip);
    }

    // set status 3432939584
    public Object setStatusDang(String name) {
        ScripNhaChinh scrip = scripRepository.findByName(name);
        scrip.setStatus("dang");
        long time = 3432939584L;
        scrip.setTimeDamNguoc(time);
        return scripRepository.save(scrip);
    }

    public Object setStatusChua(String name) {
        ScripNhaChinh scrip = scripRepository.findByName(name);
        scrip.setStatus("chua");
        return scripRepository.save(scrip);
    }

    // tạo scrip với tên
    // khởi tạo scrip với tên
    public ScripNhaChinh saveScrip(String name) {
        String result = name.replaceAll("\\n", "");
        ScripNhaChinh scrip = new ScripNhaChinh(result); // khởi tạo scrip với tên
        return scripRepository.save(scrip);
    }

    // Phương thức tìm scrip qua name
    public ScripNhaChinh findByName(String name) {
        return scripRepository.findByName(name);
    }

    // phân tích thời gian từ chuỗi không có ngày rồi cọno thêm ngày tháng hiện tại
    // đổi sang unix time
    public long changeTimeToUnixNoDay(String timeString) {
        // Phân tích chuỗi và trích xuất thông tin
        String[] timeParts = timeString.split(":");
        int hours = Integer.parseInt(timeParts[0]);
        int minutes = Integer.parseInt(timeParts[1]);
        int seconds = Integer.parseInt(timeParts[2]);

        // Tạo đối tượng LocalTime từ chuỗi đầu vào
        LocalTime localTime = LocalTime.of(hours, minutes, seconds);
        long secondsSinceMidnight = localTime.toSecondOfDay();
        System.out.println("Thời gian tương ứng secondsSinceMidnight  : " + secondsSinceMidnight);
        long currentUnixTime = Instant.now().getEpochSecond();// Lấy thời gian hiện tại dưới dạng Unix time
        return secondsSinceMidnight + currentUnixTime;// Cộng thêm thời gian hiện tại với thời gian từ chuỗi đầu vào
    }

    // nếu có thêm ngày thì sẽ quy đổi sang unix time như sau
    public long changeTimeToUnixInDay(String daysFromEpoch, String timeString) {
        // Phân tích chuỗi thời gian và trích xuất thông tin
        String[] timeParts = timeString.split(":");
        int hours = Integer.parseInt(timeParts[0]);
        int minutes = Integer.parseInt(timeParts[1]);
        int seconds = Integer.parseInt(timeParts[2]);

        // Tạo đối tượng LocalDate từ số ngày từ Epoch
        LocalDate localDate = LocalDate.ofEpochDay(Integer.parseInt(daysFromEpoch));

        // Tạo đối tượng LocalTime từ chuỗi thời gian
        LocalTime localTime = LocalTime.of(hours, minutes, seconds);

        // Tạo đối tượng Instant từ LocalDate và LocalTime
        Instant instant = localDate.atTime(localTime).atOffset(ZoneOffset.UTC).toInstant();

        // Chuyển đổi sang Unix time
        long unixTime = 0;
        unixTime = instant.getEpochSecond();

        // Cộng thêm thời gian hiện tại của máy tính

        unixTime += Instant.now().getEpochSecond();

        return unixTime;
    }

    // hàm switch case
    public String switchCase(String matchedWord, long level, long x, long y, CongTrinh congTrinh, ScripNhaChinh scrip,
            String ketQuaTraVe) {
        switch (matchedWord) {
            case "toaThiChinh":
                // level của clien và int
                if (level > congTrinh.getNhaChinh().getLevel()) {
                    x = congTrinh.getNhaChinh().getX();
                    y = congTrinh.getNhaChinh().getY();
                    ketQuaTraVe = x + "=" + y;
                } else {
                    ketQuaTraVe = "công trình đạt yêu cầu";
                    String scripDelete = xoaChuoiDauTienSQL(scrip);
                    scrip.setChuoiLuuScrip(scripDelete); // xóa đi chuỗi đầu tiên
                    String congTrinhTiepTheo = getChuoiDau(scripDelete);
                    scrip.setCongTrinhCanNangCap(congTrinhTiepTheo); // công trình kế tiêps
                    scripRepository.save(scrip);
                }

                break;
            case "benhVien":
                // level của clien và int
                if (level > congTrinh.getBenhVien().getLevel()) {
                    x = congTrinh.getBenhVien().getX();
                    y = congTrinh.getBenhVien().getY();
                    ketQuaTraVe = x + "=" + y;
                } else {
                    ketQuaTraVe = "công trình đạt yêu cầu";
                    String scripDelete = xoaChuoiDauTienSQL(scrip);
                    scrip.setChuoiLuuScrip(scripDelete);
                    String congTrinhTiepTheo = getChuoiDau(scripDelete);
                    scrip.setCongTrinhCanNangCap(congTrinhTiepTheo); // công trình kế tiêps
                    scripRepository.save(scrip);
                }
                break;
            case "nhaKetGiao":
                // level của clien và int
                if (level > congTrinh.getNhaKetGiao().getLevel()) {
                    x = congTrinh.getNhaKetGiao().getX();
                    y = congTrinh.getNhaKetGiao().getY();
                    ketQuaTraVe = x + "=" + y;
                } else {
                    ketQuaTraVe = "công trình đạt yêu cầu";
                    String scripDelete = xoaChuoiDauTienSQL(scrip);
                    scrip.setChuoiLuuScrip(scripDelete); // xóa đi chuỗi đầu tiên
                    String congTrinhTiepTheo = getChuoiDau(scripDelete);
                    scrip.setCongTrinhCanNangCap(congTrinhTiepTheo); // công trình kế tiêps
                    scripRepository.save(scrip);
                }

                break;
            case "tramDich":
                // level của clien và int
                if (level > congTrinh.getTramDich().getLevel()) {
                    x = congTrinh.getTramDich().getX();
                    y = congTrinh.getTramDich().getY();
                    ketQuaTraVe = x + "=" + y;
                } else {
                    ketQuaTraVe = "công trình đạt yêu cầu";
                    String scripDelete = xoaChuoiDauTienSQL(scrip);
                    scrip.setChuoiLuuScrip(scripDelete); // xóa đi chuỗi đầu tiên
                    String congTrinhTiepTheo = getChuoiDau(scripDelete);
                    scrip.setCongTrinhCanNangCap(congTrinhTiepTheo); // công trình kế tiêps
                    scripRepository.save(scrip);
                }
                break;
            case "tuongThanh":
                // level của clien và int
                if (level > congTrinh.getTuongThanh().getLevel()) {
                    x = congTrinh.getTuongThanh().getX();
                    y = congTrinh.getTuongThanh().getY();
                    ketQuaTraVe = x + "=" + y;
                } else {
                    ketQuaTraVe = "công trình đạt yêu cầu";
                    String scripDelete = xoaChuoiDauTienSQL(scrip);
                    scrip.setChuoiLuuScrip(scripDelete); // xóa đi chuỗi đầu tiên
                    String congTrinhTiepTheo = getChuoiDau(scripDelete);
                    scrip.setCongTrinhCanNangCap(congTrinhTiepTheo); // công trình kế tiêps
                    scripRepository.save(scrip);
                }
                break;
            case "traiTrinhSat":
                // level của clien và int
                if (level > congTrinh.getTraiTrinhSat().getLevel()) {
                    x = congTrinh.getTraiTrinhSat().getX();
                    y = congTrinh.getTraiTrinhSat().getY();
                    ketQuaTraVe = x + "=" + y;
                } else {
                    ketQuaTraVe = "công trình đạt yêu cầu";
                    String scripDelete = xoaChuoiDauTienSQL(scrip);
                    scrip.setChuoiLuuScrip(scripDelete); // xóa đi chuỗi đầu tiên
                    String congTrinhTiepTheo = getChuoiDau(scripDelete);
                    scrip.setCongTrinhCanNangCap(congTrinhTiepTheo); // công trình kế tiêps
                    scripRepository.save(scrip);
                }
                break;
            case "doanhTrai":
                // level của clien và int
                if (level > congTrinh.getBoBinh().getLevel()) {
                    x = congTrinh.getBoBinh().getX();
                    y = congTrinh.getBoBinh().getY();
                    ketQuaTraVe = x + "=" + y;
                } else {
                    ketQuaTraVe = "công trình đạt yêu cầu";
                    String scripDelete = xoaChuoiDauTienSQL(scrip);
                    scrip.setChuoiLuuScrip(scripDelete); // xóa đi chuỗi đầu tiên
                    String congTrinhTiepTheo = getChuoiDau(scripDelete);
                    scrip.setCongTrinhCanNangCap(congTrinhTiepTheo); // công trình kế tiêps
                    scripRepository.save(scrip);
                }
                break;
            case "truongBan":
                // level của clien và int
                if (level > congTrinh.getCungThu().getLevel()) {
                    x = congTrinh.getCungThu().getX();
                    y = congTrinh.getCungThu().getY();
                    ketQuaTraVe = x + "=" + y;
                } else {
                    ketQuaTraVe = "công trình đạt yêu cầu";
                    String scripDelete = xoaChuoiDauTienSQL(scrip);
                    scrip.setChuoiLuuScrip(scripDelete); // xóa đi chuỗi đầu tiên
                    String congTrinhTiepTheo = getChuoiDau(scripDelete);
                    scrip.setCongTrinhCanNangCap(congTrinhTiepTheo); // công trình kế tiêps
                    scripRepository.save(scrip);
                }
                break;
            case "chuongNgua":
                // level của clien và int
                if (level > congTrinh.getKyBinh().getLevel()) {
                    x = congTrinh.getKyBinh().getX();
                    y = congTrinh.getKyBinh().getY();
                    ketQuaTraVe = x + "=" + y;
                } else {
                    ketQuaTraVe = "công trình đạt yêu cầu";
                    String scripDelete = xoaChuoiDauTienSQL(scrip);
                    scrip.setChuoiLuuScrip(scripDelete); // xóa đi chuỗi đầu tiên
                    String congTrinhTiepTheo = getChuoiDau(scripDelete);
                    scrip.setCongTrinhCanNangCap(congTrinhTiepTheo); // công trình kế tiêps
                    scripRepository.save(scrip);
                }
                break;
            case "nhaVuKhiBaoVay":
                // level của clien và int
                if (level > congTrinh.getLinhBanDa().getLevel()) {
                    x = congTrinh.getLinhBanDa().getX();
                    y = congTrinh.getLinhBanDa().getY();
                    ketQuaTraVe = x + "=" + y;
                } else {
                    ketQuaTraVe = "công trình đạt yêu cầu";
                    String scripDelete = xoaChuoiDauTienSQL(scrip);
                    scrip.setChuoiLuuScrip(scripDelete); // xóa đi chuỗi đầu tiên
                    String congTrinhTiepTheo = getChuoiDau(scripDelete);
                    scrip.setCongTrinhCanNangCap(congTrinhTiepTheo); // công trình kế tiêps
                    scripRepository.save(scrip);
                }
                break;
            case "moĐa":
                // level của clien và int
                if (level > congTrinh.getMoDa().getLevel()) {
                    x = congTrinh.getMoDa().getX();
                    y = congTrinh.getMoDa().getY();
                    ketQuaTraVe = x + "=" + y;
                } else {
                    ketQuaTraVe = "công trình đạt yêu cầu";
                    String scripDelete = xoaChuoiDauTienSQL(scrip);
                    scrip.setChuoiLuuScrip(scripDelete); // xóa đi chuỗi đầu tiên
                    String congTrinhTiepTheo = getChuoiDau(scripDelete);
                    scrip.setCongTrinhCanNangCap(congTrinhTiepTheo); // công trình kế tiêps
                    scripRepository.save(scrip);
                }
                break;
            case "nongTrai":
                // level của clien và trạng thái scrip là (chua )
                if (level > congTrinh.getMoNgo().getLevel()) {
                    // đến đây cần trả về tọa độ tòa nhà (x,y) (311=412) và đổi trạng thái để tránh
                    // gọi 2 lần.. tức là phải đợi clent xác nhận về trạng thái (chua)
                    x = congTrinh.getMoNgo().getX();
                    y = congTrinh.getMoNgo().getY();
                    ketQuaTraVe = x + "=" + y;
                } else {
                    ketQuaTraVe = "công trình đạt yêu cầu";
                    String scripDelete = xoaChuoiDauTienSQL(scrip);
                    scrip.setChuoiLuuScrip(scripDelete); // xóa đi chuỗi đầu tiên
                    String congTrinhTiepTheo = getChuoiDau(scripDelete);
                    scrip.setCongTrinhCanNangCap(congTrinhTiepTheo); // công trình kế tiêps
                    scripRepository.save(scrip);
                }
                break;
            case "nhaMayGo":
                // level của clien và int
                if (level > congTrinh.getMoGo().getLevel()) {
                    x = congTrinh.getMoGo().getX();
                    y = congTrinh.getMoGo().getY();
                    ketQuaTraVe = x + "=" + y;
                } else {
                    ketQuaTraVe = "công trình đạt yêu cầu";
                    String scripDelete = xoaChuoiDauTienSQL(scrip);
                    scrip.setChuoiLuuScrip(scripDelete); // xóa đi chuỗi đầu tiên
                    String congTrinhTiepTheo = getChuoiDau(scripDelete);
                    scrip.setCongTrinhCanNangCap(congTrinhTiepTheo); // công trình kế tiêps
                    scripRepository.save(scrip);
                }
                break;
            case "moVang":
                // level của clien và int
                if (level > congTrinh.getMoVang().getLevel()) {
                    x = congTrinh.getMoVang().getX();
                    y = congTrinh.getMoVang().getY();
                    ketQuaTraVe = x + "=" + y;
                } else {
                    ketQuaTraVe = "công trình đạt yêu cầu";
                    String scripDelete = xoaChuoiDauTienSQL(scrip);
                    scrip.setChuoiLuuScrip(scripDelete); // xóa đi chuỗi đầu tiên
                    String congTrinhTiepTheo = getChuoiDau(scripDelete);
                    scrip.setCongTrinhCanNangCap(congTrinhTiepTheo); // công trình kế tiêps
                    scripRepository.save(scrip);
                }
                break;
            case "nhaKho":
                // level của clien và int
                if (level > congTrinh.getNhaKho().getLevel()) {
                    x = congTrinh.getNhaKho().getX();
                    y = congTrinh.getNhaKho().getY();
                    ketQuaTraVe = x + "=" + y;
                } else {
                    ketQuaTraVe = "công trình đạt yêu cầu";
                    String scripDelete = xoaChuoiDauTienSQL(scrip);
                    scrip.setChuoiLuuScrip(scripDelete); // xóa đi chuỗi đầu tiên
                    String congTrinhTiepTheo = getChuoiDau(scripDelete);
                    scrip.setCongTrinhCanNangCap(congTrinhTiepTheo); // công trình kế tiêps
                    scripRepository.save(scrip);
                }
                break;
            case "hocVien":
                // level của clien và int
                if (level > congTrinh.getNhaNghienCuu().getLevel()) {
                    x = congTrinh.getNhaNghienCuu().getX();
                    y = congTrinh.getNhaNghienCuu().getY();
                    ketQuaTraVe = x + "=" + y;
                } else {
                    ketQuaTraVe = "công trình đạt yêu cầu";
                    String scripDelete = xoaChuoiDauTienSQL(scrip); //xóa đi chuỗi đầu tiên
                    scrip.setChuoiLuuScrip(scripDelete); // xóa đi chuỗi đầu tiên và lưu lại 
                    String congTrinhTiepTheo = getChuoiDau(scripDelete);
                    scrip.setCongTrinhCanNangCap(congTrinhTiepTheo); // công trình kế tiêps
                    scripRepository.save(scrip);
                }
                break;
            case "trungTamLienMinh":
                // level của clien và int
                if (level > congTrinh.getTrungTamLienMinh().getLevel()) {
                    x = congTrinh.getTrungTamLienMinh().getX();
                    y = congTrinh.getTrungTamLienMinh().getY();
                    ketQuaTraVe = x + "=" + y;
                } else {
                    ketQuaTraVe = "công trình đạt yêu cầu";
                    String scripDelete = xoaChuoiDauTienSQL(scrip);
                    scrip.setChuoiLuuScrip(scripDelete); // xóa đi chuỗi đầu tiên
                    String congTrinhTiepTheo = getChuoiDau(scripDelete);
                    scrip.setCongTrinhCanNangCap(congTrinhTiepTheo); // công trình kế tiêps
                    scripRepository.save(scrip);
                }
                break;
            case "choPhien":
                // level của clien và int
                if (level > congTrinh.getBiaKyNiem().getLevel()) {
                    x = congTrinh.getBiaKyNiem().getX();
                    y = congTrinh.getBiaKyNiem().getY();
                    ketQuaTraVe = x + "=" + y;
                } else {
                    ketQuaTraVe = "công trình đạt yêu cầu";
                    String scripDelete = xoaChuoiDauTienSQL(scrip);
                    scrip.setChuoiLuuScrip(scripDelete); // xóa đi chuỗi đầu tiên
                    String congTrinhTiepTheo = getChuoiDau(scripDelete);
                    scrip.setCongTrinhCanNangCap(congTrinhTiepTheo); // công trình kế tiêps
                    scripRepository.save(scrip);
                }
                break;
            default:
        }
        return ketQuaTraVe;
    }

    public static String result = "";

    public static String xoaChuoiDauTienSQL(ScripNhaChinh scrip) {

        String input = scrip.getChuoiLuuScrip();
        String[] parts = input.split("\\|"); // Tách chuỗi sau mỗi dấu "|"
        int length = parts.length;// Xác định số phần tử của mảng
        if (length > 0) {
            String[] trimmedParts = new String[length - 1]; // Tạo mảng mới để lưu các phần tử sau khi loại bỏ phần tử
                                                            // đầu tiên
            // Sao chép các phần tử từ mảng parts vào mảng mới, bỏ qua ph ần tử đầu tiên
            for (int i = 1; i < length; i++) {
                trimmedParts[i - 1] = parts[i];
            }
            result = String.join("|", trimmedParts); // scrip cắt đi đoạn đầu tiên nếu lv thực tế cao hơn lv scrp
        } else {
            result = "het chuoi";
        }

        return result;
    }

    // Phương thức static để lấy chuỗi đầu tiên sau khi đã tách
    public static String getChuoiDau(String input) {
        String[] parts = input.split("\\|"); // Tách chuỗi sau mỗi dấu "|"
        String firstString = parts[0]; // Lấy chuỗi đầu tiên sau khi đã tách
        return firstString;
    }

}

//
//
//
//
