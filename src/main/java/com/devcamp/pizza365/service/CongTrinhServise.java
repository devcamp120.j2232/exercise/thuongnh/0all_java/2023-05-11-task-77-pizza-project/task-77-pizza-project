package com.devcamp.pizza365.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizza365.entity.BuildingCongTrinh;
import com.devcamp.pizza365.entity.CongTrinh;
import com.devcamp.pizza365.repository.CongTrinhRepository;
import com.google.gson.Gson;

import java.util.List;

@Service
public class CongTrinhServise {

    @Autowired
    private CongTrinhRepository congTrinhRepository;

    private String convertToJsonString(Object object) {
        Gson gson = new Gson();
        return gson.toJson(object);
    }

    public List<CongTrinh> getAllCongTrinhs() {
        return congTrinhRepository.findAll();
    }

    // dịch vụ xuất giá trị của các thuộc tính trong công trình
    public String getBuildingDetailsOfCongTrinhByName(String name, String buildingType, String properType) {
        CongTrinh congTrinh = congTrinhRepository.findByName(name);
        if (congTrinh != null) {
            BuildingCongTrinh building = null;
            switch (buildingType) {
                case "nhaChinh":
                    building = congTrinh.getNhaChinh();
                    break;
                case "benhVien":
                    building = congTrinh.getBenhVien();
                    break;
                case "nhaKetGiao":
                    building = congTrinh.getNhaKetGiao();
                    break;
                case "tramDich":
                    building = congTrinh.getTramDich();
                    break;
                case "tuongThanh":
                    building = congTrinh.getTuongThanh();
                    break;
                case "traiTrinhSat":
                    building = congTrinh.getTraiTrinhSat();
                    break;
                case "boBinh":
                    building = congTrinh.getBoBinh();
                    break;
                case "cungThu":
                    building = congTrinh.getCungThu();
                    break;
                case "kyBinh":
                    building = congTrinh.getKyBinh();
                    break;
                case "linhBanDa":
                    building = congTrinh.getLinhBanDa();
                    break;
                case "moDa":
                    building = congTrinh.getMoDa();
                    break;
                case "moNgo":
                    building = congTrinh.getMoNgo();
                    break;
                case "moGo":
                    building = congTrinh.getMoGo();
                    break;
                case "moVang":
                    building = congTrinh.getMoVang();
                    break;
                case "nhaKho":
                    building = congTrinh.getNhaKho();
                    break;
                case "nhaNghienCuu":
                    building = congTrinh.getNhaNghienCuu();
                    break;
                case "trungTamLienMinh":
                    building = congTrinh.getTrungTamLienMinh();
                    break;
                case "biaKyNiem":
                    building = congTrinh.getBiaKyNiem();
                    break;
                // Thêm các trường hợp khác nếu cần
                default:
                    // Trả về null nếu không tìm thấy loại đối tượng
                    return "buildingType nhập sai!";
            }
            if (building != null) {
                // Chuyển đối tượng building thành chuỗi JSON và trả về
                // Kiểm tra properType để xác định thuộc tính cần trả về
                switch (properType) {
                    case "x":
                        return convertToJsonString(building.getX());
                    case "y":
                        return convertToJsonString(building.getY());
                    case "level":
                        return convertToJsonString(building.getLevel());

                    case "hanh_dong":
                        return convertToJsonString(building.gethanhDong());
                    default:
                        // Trả về đối tượng đầy đủ thuộc tính nếu
                        return "properType nhập sai! {x, y, level, hanh_dong}";
                }

            }
        }
        return "công trình {buildingType} tìm được khong_co_gia_tri";
    }

    //// Set công trình bằng từng thuộc tính 1

    public CongTrinh saveCongTrinh(CongTrinh congTrinh) {
        return congTrinhRepository.save(congTrinh);
    }
    // khởi tạo công trình với tên
    public CongTrinh saveCongTrinh(String name) {
        // tiền xử lý chuỗi 
        CongTrinh congTrinh2 = new CongTrinh(name); // khởi tạo công trình với tên
        return congTrinhRepository.save(congTrinh2);
    }

    public CongTrinh getCongTrinhById(Long id) {
        return congTrinhRepository.findById(id).orElse(null);
    }

    public void deleteCongTrinhById(Long id) {
        congTrinhRepository.deleteById(id);
    }

    // Phương thức tìm công trình qua name
    public CongTrinh findByName(String name) {
        return congTrinhRepository.findByName(name);
    }
}