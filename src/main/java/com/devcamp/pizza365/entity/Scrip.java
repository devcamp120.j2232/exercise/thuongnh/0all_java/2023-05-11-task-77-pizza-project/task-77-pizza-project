package com.devcamp.pizza365.entity;

import javax.persistence.*;

@Entity
@Table(name = "scrip") // Xác định tên bảng trong cơ sở dữ liệu
public class Scrip {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true) // Đảm bảo trường "name" là duy nhất
    private String name; // têb cuar nick

    @Column(name = "chuoi_luu_scrip")
    private String chuoiLuuScrip;

    @Column(name = "cong_trinh_can_nang_cap")
    private String congTrinhCanNangCap;

    @Column(name = "time_dem_nguoc")
    private Long timeDamNguoc;

    private String status;

    private Long level_now;

    private Long phutDemNguoc;

    // Getters and setters
    // Constructors

    // Constructors
    public Scrip() {
    }

    public void setLevel_now(Long level_now) {
        this.level_now = level_now;
    }

    public Long getPhutDemNguoc() {
        return phutDemNguoc;
    }

    public void setPhutDemNguoc(Long phutDemNguoc) {
        this.phutDemNguoc = phutDemNguoc;
    }

    public Scrip(String name) {
        this.name = name;
        this.chuoiLuuScrip = "nong Trai>7|doanh Trai>7|trung Tam Lien Minh > 8|hoc Vien >9|chuong Ngua >10|benh Vien >10|nha Kho >11|nha May Go >12|truong ban >12|trung Tam Lien Minh > 13|mo Vang > 13|cho Phien > 13|mo Vang > 14|hoc Vien > 15|nong Trai > 16|doanh Trai > 16|benh Vien > 16 ";
        
        this.congTrinhCanNangCap = "nong Trai>7";
        this.timeDamNguoc = (long) 1;
        this.status = "chua"; // (chua,dang,timeout,đã) chưa nâng cấp , đang nâng cấp , time out , đã nâng cấp   
        this.level_now = (long) 1;
        this.phutDemNguoc = (long) 0;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChuoiLuuScrip() {
        return chuoiLuuScrip;
    }

    public void setChuoiLuuScrip(String chuoiLuuScrip) {
        this.chuoiLuuScrip = chuoiLuuScrip;
    }

    public String getCongTrinhCanNangCap() {
        return congTrinhCanNangCap;
    }

    public void setCongTrinhCanNangCap(String congTrinhCanNangCap) {
        this.congTrinhCanNangCap = congTrinhCanNangCap;
    }

    public Long getTimeDamNguoc() {
        return timeDamNguoc;
    }

    public void setTimeDamNguoc(Long timeDamNguoc) {
        this.timeDamNguoc = timeDamNguoc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getLevel_now() {
        return level_now;
    }

    @Override
    public String toString() {
        return "Scrip [id=" + id + ", name=" + name + ", chuoiLuuScrip=" + chuoiLuuScrip + ", congTrinhCanNangCap="
                + congTrinhCanNangCap + ", timeDamNguoc=" + timeDamNguoc + ", status=" + status + ", level_now="
                + level_now + "]";
    }


}
