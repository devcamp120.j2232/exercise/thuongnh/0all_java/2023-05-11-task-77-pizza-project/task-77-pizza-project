package com.devcamp.pizza365.entity;

import javax.persistence.Embeddable;

import javax.persistence.Column;
import javax.persistence.Table;

@Embeddable
@Table(name = "building_congtrinh") // Xác định tên bảng trong cơ sở dữ liệu
public class BuildingCongTrinh {
    @Column(name = "x")
    private int x;

    @Column(name = "y")
    private int y;

    @Column(name = "hanh_dong")
    private String hanhDong;

    @Column(name = "level")
    private int level;

    // Getters and setters
    // Constructors

      // Constructors
      public BuildingCongTrinh() {
    }

    public BuildingCongTrinh(int x, int y, String hanhDong, int level) {
        this.x = x;
        this.y = y;
        this.hanhDong = hanhDong;
        this.level = level;
    }

    // Getters and setters
    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String gethanhDong() {
        return hanhDong;
    }

    public void sethanhDongn(String hanhDong) {
        this.hanhDong = hanhDong;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }


}
