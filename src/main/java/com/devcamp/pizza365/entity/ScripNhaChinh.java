package com.devcamp.pizza365.entity;

import javax.persistence.*;

@Entity
@Table(name = "scrip_nha_chinh") // Xác định tên bảng trong cơ sở dữ liệu
public class ScripNhaChinh {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(unique = true) // Đảm bảo trường "name" là duy nhất
    private String name; // têb cuar nick

    @Column(name = "chuoi_luu_scrip")
    private String chuoiLuuScrip;

    @Column(name = "cong_trinh_can_nang_cap")
    private String congTrinhCanNangCap;

    @Column(name = "time_dem_nguoc")
    private Long timeDamNguoc;

    private String status;

    private Long level_now;

    private Long phutDemNguoc;

    // Getters and setters
    // Constructors

    // Constructors
    public ScripNhaChinh() {
    }

    public void setLevel_now(Long level_now) {
        this.level_now = level_now;
    }

    public Long getPhutDemNguoc() {
        return phutDemNguoc;
    }

    public void setPhutDemNguoc(Long phutDemNguoc) {
        this.phutDemNguoc = phutDemNguoc;
    }

    public ScripNhaChinh(String name) {
        this.name = name;
        this.chuoiLuuScrip = "mo da>7|nha Ket Giao>7|tuong Thanh>7|nha Chinh>7|mo da>8|nha Ket Giao>8|tuong Thanh>8|nha Chinh>8|mo da>9|nha Ket Giao>9|tuong Thanh>9|nha Chinh>9|mo da>10|nha Ket Giao>10|tuong Thanh>10|nha Chinh>10|mo da>11|nha Ket Giao>11|tuong Thanh>11|nha Chinh>11|mo da>12|nha Ket Giao>12|tuong Thanh>12|nha Chinh>12|mo da>13|nha Ket Giao>13|tuong Thanh>13|nha Chinh>13|mo da>14|nha Ket Giao>14|tuong Thanh>14|nha Chinh>14|mo da>15|nha Ket Giao>15|tuong Thanh>15|nha Chinh>15|mo da>16|nha Ket Giao>16|tuong Thanh>16|nha Chinh>16|mo da>17|nha Ket Giao>17|tuong Thanh>17|nha Chinh>17|mo da>18|nha Ket Giao>18|tuong Thanh>18|nha Chinh>18|";
        
        this.congTrinhCanNangCap = "mo da>7";
        this.timeDamNguoc = (long) 1;
        this.status = "chua"; // (chua,dang,timeout,đã) chưa nâng cấp , đang nâng cấp , time out , đã nâng cấp   
        this.level_now = (long) 1;
        this.phutDemNguoc = (long) 0;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getChuoiLuuScrip() {
        return chuoiLuuScrip;
    }

    public void setChuoiLuuScrip(String chuoiLuuScrip) {
        this.chuoiLuuScrip = chuoiLuuScrip;
    }

    public String getCongTrinhCanNangCap() {
        return congTrinhCanNangCap;
    }

    public void setCongTrinhCanNangCap(String congTrinhCanNangCap) {
        this.congTrinhCanNangCap = congTrinhCanNangCap;
    }

    public Long getTimeDamNguoc() {
        return timeDamNguoc;
    }

    public void setTimeDamNguoc(Long timeDamNguoc) {
        this.timeDamNguoc = timeDamNguoc;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Long getLevel_now() {
        return level_now;
    }

    @Override
    public String toString() {
        return "Scrip [id=" + id + ", name=" + name + ", chuoiLuuScrip=" + chuoiLuuScrip + ", congTrinhCanNangCap="
                + congTrinhCanNangCap + ", timeDamNguoc=" + timeDamNguoc + ", status=" + status + ", level_now="
                + level_now + "]";
    }


}
