package com.devcamp.pizza365.entity;

import javax.persistence.*;

@Entity
@Table(name = "cong_trinh")
public class CongTrinh {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    
    @Column(unique = true) // Đảm bảo trường "name" là duy nhất
    private String name;  // têb cuar nick 

    // 1 nhà chính
    @Embedded // nhúng từ building_congtrinh
    @AttributeOverrides({
            @AttributeOverride(name = "x", column = @Column(name = "nha_chinh_x")),
            @AttributeOverride(name = "y", column = @Column(name = "nha_chinh_y")),
            @AttributeOverride(name = "hanhDong", column = @Column(name = "nha_chinh_hanhDong")),
            @AttributeOverride(name = "level", column = @Column(name = "nha_chinh_level"))
    })
    private BuildingCongTrinh nhaChinh;

     // 1 nhà chính
     @Embedded // nhúng từ building_congtrinh
     @AttributeOverrides({
             @AttributeOverride(name = "x", column = @Column(name = "benh_vien_x")),
             @AttributeOverride(name = "y", column = @Column(name = "benh_vien_y")),
             @AttributeOverride(name = "hanhDong", column = @Column(name = "benh_vien_hanhDong")),
             @AttributeOverride(name = "level", column = @Column(name = "benh_vien_level"))
     })
     private BuildingCongTrinh benhVien;

    // 2 nhà kết giao
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "x", column = @Column(name = "nha_ket_giao_x")),
            @AttributeOverride(name = "y", column = @Column(name = "nha_ket_giao_y")),
            @AttributeOverride(name = "hanhDong", column = @Column(name = "nha_ket_giao_hanhDong")),
            @AttributeOverride(name = "level", column = @Column(name = "nha_ket_giao_level"))
    })
    private BuildingCongTrinh nhaKetGiao;
    // 3 trạm dịch
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "x", column = @Column(name = "tram_dich_x")),
            @AttributeOverride(name = "y", column = @Column(name = "tram_dich_y")),
            @AttributeOverride(name = "hanhDong", column = @Column(name = "tram_dich_hanhDong")),
            @AttributeOverride(name = "level", column = @Column(name = "tram_dich_level"))
    })
    private BuildingCongTrinh tramDich;
    // 4 tường thành
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "x", column = @Column(name = "tuong_thanh_x")),
            @AttributeOverride(name = "y", column = @Column(name = "tuong_thanh_y")),
            @AttributeOverride(name = "hanhDong", column = @Column(name = "tuong_thanh_hanhDong")),
            @AttributeOverride(name = "level", column = @Column(name = "tuong_thanh_level"))
    })
    private BuildingCongTrinh tuongThanh;
    // 5 trại trinh sát
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "x", column = @Column(name = "trai_trinh_sat_x")),
            @AttributeOverride(name = "y", column = @Column(name = "trai_trinh_sat_y")),
            @AttributeOverride(name = "hanhDong", column = @Column(name = "trai_trinh_sat_hanhDong")),
            @AttributeOverride(name = "level", column = @Column(name = "trai_trinh_sat_level"))
    })
    private BuildingCongTrinh traiTrinhSat;
    // 6 bộ binh
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "x", column = @Column(name = "bo_binh_x")),
            @AttributeOverride(name = "y", column = @Column(name = "bo_binh_y")),
            @AttributeOverride(name = "hanhDong", column = @Column(name = "bo_binh_hanhDong")),
            @AttributeOverride(name = "level", column = @Column(name = "bo_binh_level"))
    })
    private BuildingCongTrinh boBinh;
    // 7 cung thủ
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "x", column = @Column(name = "cung_thu_x")),
            @AttributeOverride(name = "y", column = @Column(name = "cung_thu_y")),
            @AttributeOverride(name = "hanhDong", column = @Column(name = "cung_thu_hanhDong")),
            @AttributeOverride(name = "level", column = @Column(name = "cung_thu_level"))
    })
    private BuildingCongTrinh cungThu;
    // 8 kỵ binh
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "x", column = @Column(name = "ky_binh_x")),
            @AttributeOverride(name = "y", column = @Column(name = "ky_binh_y")),
            @AttributeOverride(name = "hanhDong", column = @Column(name = "ky_binh_hanhDong")),
            @AttributeOverride(name = "level", column = @Column(name = "ky_binh_level"))
    })
    private BuildingCongTrinh kyBinh;
    // 9 lính bắn đá
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "x", column = @Column(name = "linh_ban_da_x")),
            @AttributeOverride(name = "y", column = @Column(name = "linh_ban_da_y")),
            @AttributeOverride(name = "hanhDong", column = @Column(name = "linh_ban_da_hanhDong")),
            @AttributeOverride(name = "level", column = @Column(name = "linh_ban_da_level"))
    })
    private BuildingCongTrinh linhBanDa;
    // 10 mỏ đá
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "x", column = @Column(name = "mo_da_x")),
            @AttributeOverride(name = "y", column = @Column(name = "mo_da_y")),
            @AttributeOverride(name = "hanhDong", column = @Column(name = "mo_da_hanhDong")),
            @AttributeOverride(name = "level", column = @Column(name = "mo_da_level"))
    })
    private BuildingCongTrinh moDa;
    // 11 mỏ ngô
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "x", column = @Column(name = "mo_ngo_x")),
            @AttributeOverride(name = "y", column = @Column(name = "mo_ngo_y")),
            @AttributeOverride(name = "hanhDong", column = @Column(name = "mo_ngo_hanhDong")),
            @AttributeOverride(name = "level", column = @Column(name = "mo_ngo_level"))
    })
    private BuildingCongTrinh moNgo;
    // 12 mỏ gỗ
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "x", column = @Column(name = "mo_go_x")),
            @AttributeOverride(name = "y", column = @Column(name = "mo_go_y")),
            @AttributeOverride(name = "hanhDong", column = @Column(name = "mo_go_hanhDong")),
            @AttributeOverride(name = "level", column = @Column(name = "mo_go_level"))
    })
    private BuildingCongTrinh moGo;
    // 13 mỏ vàng
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "x", column = @Column(name = "mo_vang_x")),
            @AttributeOverride(name = "y", column = @Column(name = "mo_vang_y")),
            @AttributeOverride(name = "hanhDong", column = @Column(name = "mo_vang_hanhDong")),
            @AttributeOverride(name = "level", column = @Column(name = "mo_vang_level"))
    })
    private BuildingCongTrinh moVang;
    // 14 nhà kho
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "x", column = @Column(name = "nha_kho_x")),
            @AttributeOverride(name = "y", column = @Column(name = "nha_kho_y")),
            @AttributeOverride(name = "hanhDong", column = @Column(name = "nha_kho_hanhDong")),
            @AttributeOverride(name = "level", column = @Column(name = "nha_kho_level"))
    })
    private BuildingCongTrinh nhaKho;
    // 15 nhà nghiên cứu
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "x", column = @Column(name = "nha_nghien_cuu_x")),
            @AttributeOverride(name = "y", column = @Column(name = "nha_nghien_cuu_y")),
            @AttributeOverride(name = "hanhDong", column = @Column(name = "nha_nghien_cuu_hanhDong")),
            @AttributeOverride(name = "level", column = @Column(name = "nha_nghien_cuu_level"))
    })
    private BuildingCongTrinh nhaNghienCuu;
    // 16 trung tâm lien minh
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "x", column = @Column(name = "trung_tam_lien_minh_x")),
            @AttributeOverride(name = "y", column = @Column(name = "trung_tam_lien_minh_y")),
            @AttributeOverride(name = "hanhDong", column = @Column(name = "trung_tam_lien_minh_hanhDong")),
            @AttributeOverride(name = "level", column = @Column(name = "trung_tam_lien_minh_level"))
    })
    private BuildingCongTrinh trungTamLienMinh;
    // 17 bia kỷ niệm
    @Embedded
    @AttributeOverrides({
            @AttributeOverride(name = "x", column = @Column(name = "cho_phien_x")),
            @AttributeOverride(name = "y", column = @Column(name = "cho_phien_y")),
            @AttributeOverride(name = "hanhDong", column = @Column(name = "cho_phien_hanhDong")),
            @AttributeOverride(name = "level", column = @Column(name = "cho_phien_level"))
    })
    private BuildingCongTrinh biaKyNiem;

    // Getters and setters
    // Constructors

    // Constructors

    public CongTrinh() {
    }
    public CongTrinh(String name) {
        this.name = name;
        this.nhaChinh = new BuildingCongTrinh(0,0,"_",  0);
        this.nhaKetGiao = new BuildingCongTrinh(0,0,"_",  0);
        this.tramDich = new BuildingCongTrinh(0,0,"_",  0);
        this.tuongThanh = new BuildingCongTrinh(555,331,"_",  0);
        this.traiTrinhSat = new BuildingCongTrinh(0,0,"_",  0);
        this.boBinh = new BuildingCongTrinh(0,0,"_",  0);
        this.cungThu = new BuildingCongTrinh(0,0,"_",  0);
        this.kyBinh = new BuildingCongTrinh(0,0,"_",  0);
        this.linhBanDa = new BuildingCongTrinh(0,0,"_",  0);
        this.moDa = new BuildingCongTrinh(0,0,"_",  0);
        this.moNgo = new BuildingCongTrinh(0,0,"_",  0);
        this.moGo = new BuildingCongTrinh(0,0,"_",  0);
        this.moVang = new BuildingCongTrinh(0,0,"_",  0);
        this.nhaKho = new BuildingCongTrinh(0,0,"_",  0);
        this.nhaNghienCuu = new BuildingCongTrinh(0,0,"_",  0);
        this.trungTamLienMinh = new BuildingCongTrinh(0,0,"_",  0);
        this.biaKyNiem = new BuildingCongTrinh(0,0,"_",  0);
        this.benhVien =      new BuildingCongTrinh(0,0,"_",  0);
    }

    public CongTrinh(Long id, String name, BuildingCongTrinh nhaChinh, BuildingCongTrinh nhaKetGiao, BuildingCongTrinh tramDich,
            BuildingCongTrinh tuongThanh, BuildingCongTrinh traiTrinhSat, BuildingCongTrinh boBinh,
            BuildingCongTrinh cungThu, BuildingCongTrinh kyBinh, BuildingCongTrinh linhBanDa, BuildingCongTrinh moDa,
            BuildingCongTrinh moNgo, BuildingCongTrinh moGo, BuildingCongTrinh moVang, BuildingCongTrinh nhaKho,
            BuildingCongTrinh nhaNghienCuu, BuildingCongTrinh trungTamLienMinh, BuildingCongTrinh biaKyNiem) {
        this.id = id;
        this.name = name;
        this.nhaChinh = nhaChinh;
        this.nhaKetGiao = nhaKetGiao;
        this.tramDich = tramDich;
        this.tuongThanh = tuongThanh;
        this.traiTrinhSat = traiTrinhSat;
        this.boBinh = boBinh;
        this.cungThu = cungThu;
        this.kyBinh = kyBinh;
        this.linhBanDa = linhBanDa;
        this.moDa = moDa;
        this.moNgo = moNgo;
        this.moGo = moGo;
        this.moVang = moVang;
        this.nhaKho = nhaKho;
        this.nhaNghienCuu = nhaNghienCuu;
        this.trungTamLienMinh = trungTamLienMinh;
        this.biaKyNiem = biaKyNiem;
    }

    // Getters and setters

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String  getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BuildingCongTrinh getNhaChinh() {
        return nhaChinh;
    }

    public void setNhaChinh(BuildingCongTrinh nhaChinh) {
        this.nhaChinh = nhaChinh;
    }

    public BuildingCongTrinh getNhaKetGiao() {
        return nhaKetGiao;
    }

    public void setBenhVien(BuildingCongTrinh benhVien) {
        this.benhVien = benhVien;
    }

    public BuildingCongTrinh getBenhVien() {
        return benhVien;
    }

    public void setNhaKetGiao(BuildingCongTrinh nhaKetGiao) {
        this.nhaKetGiao = nhaKetGiao;
    }

    public BuildingCongTrinh getTramDich() {
        return tramDich;
    }

    public void setTramDich(BuildingCongTrinh tramDich) {
        this.tramDich = tramDich;
    }

    public BuildingCongTrinh getTuongThanh() {
        return tuongThanh;
    }

    public void setTuongThanh(BuildingCongTrinh tuongThanh) {
        this.tuongThanh = tuongThanh;
    }

    public BuildingCongTrinh getTraiTrinhSat() {
        return traiTrinhSat;
    }

    public void setTraiTrinhSat(BuildingCongTrinh traiTrinhSat) {
        this.traiTrinhSat = traiTrinhSat;
    }

    public BuildingCongTrinh getBoBinh() {
        return boBinh;
    }

    public void setBoBinh(BuildingCongTrinh boBinh) {
        this.boBinh = boBinh;
    }

    public BuildingCongTrinh getCungThu() {
        return cungThu;
    }

    public void setCungThu(BuildingCongTrinh cungThu) {
        this.cungThu = cungThu;
    }

    public BuildingCongTrinh getKyBinh() {
        return kyBinh;
    }

    public void setKyBinh(BuildingCongTrinh kyBinh) {
        this.kyBinh = kyBinh;
    }

    public BuildingCongTrinh getLinhBanDa() {
        return linhBanDa;
    }

    public void setLinhBanDa(BuildingCongTrinh linhBanDa) {
        this.linhBanDa = linhBanDa;
    }

    public BuildingCongTrinh getMoDa() {
        return moDa;
    }

    public void setMoDa(BuildingCongTrinh moDa) {
        this.moDa = moDa;
    }

    public BuildingCongTrinh getMoNgo() {
        return moNgo;
    }

    public void setMoNgo(BuildingCongTrinh moNgo) {
        this.moNgo = moNgo;
    }

    public BuildingCongTrinh getMoGo() {
        return moGo;
    }

    public void setMoGo(BuildingCongTrinh moGo) {
        this.moGo = moGo;
    }

    public BuildingCongTrinh getMoVang() {
        return moVang;
    }

    public void setMoVang(BuildingCongTrinh moVang) {
        this.moVang = moVang;
    }

    public BuildingCongTrinh getNhaKho() {
        return nhaKho;
    }

    public void setNhaKho(BuildingCongTrinh nhaKho) {
        this.nhaKho = nhaKho;
    }

    public BuildingCongTrinh getNhaNghienCuu() {
        return nhaNghienCuu;
    }

    public void setNhaNghienCuu(BuildingCongTrinh nhaNghienCuu) {
        this.nhaNghienCuu = nhaNghienCuu;
    }

    public BuildingCongTrinh getTrungTamLienMinh() {
        return trungTamLienMinh;
    }

    public void setTrungTamLienMinh(BuildingCongTrinh trungTamLienMinh) {
        this.trungTamLienMinh = trungTamLienMinh;
    }

    public BuildingCongTrinh getBiaKyNiem() {
        return biaKyNiem;
    }

    public void setBiaKyNiem(BuildingCongTrinh biaKyNiem) {
        this.biaKyNiem = biaKyNiem;
    }

   
}

// Getters and setters
